# CMOC Handover

This project houses the issues that are created to transition the currently on-call CMOC to the next one. Issues should be opened using the [Handover](https://gitlab.com/gitlab-com/support/dotcom/cmoc-handover/-/blob/master/.gitlab/issue_templates/Default.md) template.

Wondering what a CMOC is? [Read all about it](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#roles-and-responsibilities).

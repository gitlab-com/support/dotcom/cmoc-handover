<!-- 

Please title this issue "Handover (REGION)"

As the Outgoing CMOC, use your region in the issue title. 

EMEA: https://gitlab.pagerduty.com/schedules#P59382D
AMER: https://gitlab.pagerduty.com/schedules#PG0SHU2
APAC 1: https://gitlab.pagerduty.com/schedules#PGUP5OB
APAC 2: https://gitlab.pagerduty.com/schedules#PMPKHZN

-->

**Outgoing CMOC**: @
<!-- Tag yourself -->

**Incoming CMOC**: @
/assign @
<!-- Tag and assign the incoming CMOC -->

# :night_with_stars: Outgoing CMOC Section

<!-- Outgoing CMOC, please complete this section -->

## Status Page Health Check

- [] Check this task off if the [Status Page](https://status.gitlab.com/) looks accurate at the end of your shift, meaning there are no incidents or maintenance events on it that shouldn't be there.

## Checklist

<!--
For the rest of the checklist use checkboxes to explain the state:

- [~] Disabled means "nothing to handover".
- [x] Checked means "something to handover".

If there is something to handover, append the appropriate section with the following template and status:

  - Link:
    - Status: {-Ongoing-} / **_Upcoming_** / {+Resolved+}
    - Summary:

GitLab Dedicated Template:

  - Ticket:
    - Links to slack threads / issues to keep an eye on:
    - Summary of where we are at:
    - Region of customer:
-->

- [] :warning: **Incidents**



- [] :hammer_and_wrench: **Maintenances**



- [] :mailbox_with_mail: **Contact Requests**



- [] :computer: **GitLab Dedicated**



# :sunrise: Incoming CMOC Section

<!-- Incoming CMOC, please complete this section -->

Check the below task off if **all** of the following are true:

- You are able to receive and acknowledge PagerDuty pages on your device(s). You can test this in PagerDuty by going to PagerDuty.com in your browser and navigating to `My Profile > Contact Information`.
- You are able to receive notifications of your Slack mentions on your device(s). You can view [this Slack Help Center page](https://slack.com/intl/en-gb/help/articles/360001559367-Troubleshoot-Slack-notifications#not-receiving-notifications) for troubleshooting instructions.
- If you had to join an incident room now, you are set up to be able to listen to and speak with others on the call.

- [ ] **READY TO GO!**

/due today
/label ~"CMOC::Handover"
/confidential
